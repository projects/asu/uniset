#! /bin/sh

autoreconf -fiv

CFLAGS="-pipe -O2 -march=i586"
[[ `uname -m` == "x86_64" ]] && CFLAGS='-pipe -O2'

export CFLAGS
CXXFLAGS="$CFLAGS"
export CXXFLAGS

./configure --enable-maintainer-mode --prefix=/usr $*
